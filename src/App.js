import React, { useState, useRef, useEffect } from 'react';
import './App.scss';

function App() {
  const [current, setCurrent] = useState(6);
  const avatars = [
    {
      id: 1,
      img: '/img/avatar-circle-1.png',
      bgColor: 'rgb(120,121,241)'
    },
    {
      id: 2,
      img: '/img/avatar-circle-2.png',
      bgColor: 'rgb(238,73,158)'
    },
    {
      id: 3,
      img: '/img/avatar-circle-3.png',
      bgColor: 'rgb(108,219,255)'
    },
    {
      id: 4,
      img: '/img/avatar-circle-4.png',
      bgColor: 'rgb(107,255,143)'
    },
    {
        id: 5,
        img: '/img/avatar-circle-5.png',
        bgColor: 'rgb(252,97,97)'
    },
    {
        id: 6,
        img: '/img/avatar-circle-6.png',
        bgColor: 'rgba(222, 75, 62, 0.2)'
    }
  ];
  const wrap = useRef(null);
  const items = useRef([]);

  const auto = () => {
    clearInterval(timer);
    if (current === 1) {
      setCurrent(6);
    } else {
      setCurrent(current - 1);
    }
  }

  const xPos = (item) => {
    let number = parseInt(item.getAttribute('data-key'));
    if (number === current) {
      return wrap.current.offsetWidth * 0.25;
    } else if (
        (number === current - 2) ||
        (number === current - 1) ||
        (current === 2 && number === 6) ||
        (current === 1 && (number === 5 || number === 6))) {
      return 0;
    } else if (number === current + 2 || number === current + 1) {
      return wrap.current.offsetWidth * 0.5;
    } else if (
        (current === 2 && (number === 3 || number === 4)) ||
        (current === 1 && (number === 2 || number === 3)) ||
        (current === 6 && (number === 1 || number === 2)) ||
        (current === 5 && number === 1)) {
      return wrap.current.offsetWidth * 0.5;
    } else {
      return wrap.current.offsetWidth * 0.25;
    }
  }
  const yPos = (item) => {
    let number = parseInt(item.getAttribute('data-key'));
    if (number === current) {
      return wrap.current.offsetHeight * 0.05;
    } else if (
        (number === current + 1) ||
        (number === current - 1) ||
        (current === 1 && number === 6) ||
        (current === 6 && number === 1)) {
      return 0;
    } else {
      return wrap.current.offsetHeight * 0.25;
    }
  }

  const updatePositions = () => {
    items.current.forEach(item => {
      item.style.left = xPos(item) + `px`;
      item.style.top = yPos(item) + `px`;
    })
  }

  const timer = setInterval(auto, 3000);

  useEffect(() => {
    wrap.current.style.height = (wrap.current.offsetWidth + (wrap.current.offsetWidth * 0.195) + 41) + `px`;
    items.current.forEach(item => {
      item.style.width = (wrap.current.offsetWidth / 6 - 12) + `px`;
      item.style.height = (wrap.current.offsetWidth / 6 - 12 + 2) + `px`;
    })
    updatePositions();
  });

  return (
      <div className={`avatars-circle avatars-circle--current-` + current} ref={wrap}>
        {avatars.map((obj, index) => {
          return (
              <div
                  key={obj.id}
                  data-key={obj.id}
                  ref={el => items.current[index] = el}
                  className={`avatars-circle_item` + (current === obj.id ? ` active` : ``)}
                  style={{
                    backgroundColor: obj.bgColor
                  }}
              />
          )
        })}
      </div>
  );
}

export default App;
